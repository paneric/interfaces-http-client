<?php

declare(strict_types=1);

namespace Paneric\Interfaces\HttpClient;

interface HttpClientInterface
{
    public function getJsonResponse(string $method, string $url, array $options): array;
}

